module Tree where
data Tree a = T a [Tree a]

foldrT :: (a -> z -> z) -> z -> Tree a -> z
foldrT f z (T v ts) = f v $ foldr (flip $ foldrT f) z ts


t = T 1 [T 2 [T 3 []], T 4 []]