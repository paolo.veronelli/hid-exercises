{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Chapter_1.Solved.HitParade where

import Data.FingerTree
import Data.Map (Map)
import qualified Data.Map.Strict as M
import Data.Semigroup (Max (Max))
import Protolude
import Test.Hspec (describe, hspec, it, shouldBe)

-- how many times a song has been voted
newtype Score = Score Int
  deriving (Ord, Eq, Show, Bounded, Num)

-- song name
newtype Song = Song Text deriving (Eq, Ord, Show, IsString)

-- a song together with its score
data Hit = Hit
  { song :: Song
  , score :: Score
  }
  deriving (Eq, Show)

-- Hit ordering
type SortHit = (Down Song, Score)

-- project Hit into it's natural ordering
sortHit :: Hit -> SortHit
sortHit (Hit song score) = (Down song, score)

-- open and close Last

-- how to measure a Hit in a fingertree sense, checkout the measure must be a monoid
instance Measured (Last SortHit) Hit where
  measure (Just . sortHit -> h) = Last h

-- a fingertree where the monoid is an Last
type Ordered b a = FingerTree (Last b) a

-- song scores together with a fingertree for logarithmic time operations
data HitParade = HitParade
  { scores :: Map Song Score
  , parade :: Ordered SortHit Hit
  }
  deriving (Show)

noParade :: HitParade
noParade = HitParade mempty mempty

-- here is the plan where  n == number of songs
-- 1) get the song score , O (log n)
-- 2) remove the Hit (song + score) from the fingertree, O(log n)
-- 3) update the hit score  and insert it back into the fingertree, O(log n)
-- 4) update the scores , O (log n)
addVote :: Song -> HitParade -> HitParade
addVote song HitParade {..} =
  let oldScore = M.lookup song scores
      old = sortHit . Hit song <$> oldScore
      newScore = fromMaybe 0 oldScore + 1
      new = Hit song $ newScore
   in HitParade (M.insert song newScore . M.delete song $ scores) $
        insert new (sortHit new) $ maybe identity remove old parade

-- extract 'k' highest scoring Song , O (1) * k
hits :: Int -> HitParade -> [Hit]
hits n (HitParade _ parade) = take n $ toList parade

main :: IO ()
main = hspec do
  describe "hits" do
    it "handles 1 votes" do
      shouldBe
        do hits 10 (addVote "route 66" noParade)
        do [Hit "route 66" 1]
    it "handles 2 votes for same song" do
      shouldBe
        do hits 10 (addVote "route 66" $ addVote "route 66" noParade)
        do [Hit "route 66" 2]
    it "handles 2 votes for different song, lexicographically swapped" do
      shouldBe
        do hits 10 (addVote "message in a bottle" $ addVote "route 66" noParade)
        do [Hit "route 66" 1, Hit "message in a bottle" 1]
    it "handles 2 votes for different song, lexicographically straight" do
      shouldBe
        do hits 10 (addVote "route 66" $ addVote "message in a bottle" noParade)
        do [Hit "route 66" 1, Hit "message in a bottle" 1]
    it "handles 3 votes for 2 different songs" do
      shouldBe
        do hits 10 (addVote "route 66" $ addVote "route 66" $ addVote "message in a bottle" noParade)
        do [Hit "route 66" 2, Hit "message in a bottle" 1]

cutter :: Ord b => b -> Last b -> Bool
cutter h (Last e) = maybe False (>= h) e

-- extract 'k' highest scoring Song , O (1) * k
insert :: (Measured (Last b) a, Ord b) => a -> b -> FingerTree (Last b) a -> FingerTree (Last b) a
insert x y xs =
  let (pre, post) = split (cutter y) xs
   in pre <> (x <| post)

remove :: (Measured (Last b) a, Ord b) => b -> FingerTree (Last b) a -> FingerTree (Last b) a
remove y xs =
  let (pre, viewl -> _ :< post) = split (cutter y) xs
   in pre <> post
