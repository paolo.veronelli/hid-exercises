{-# LANGUAGE TypeApplications #-}

module Chapter_1.Solved.Vocabulary where

import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Protolude (Text, comparing, foldl', group, groupBy, minimumBy, on, sort, sortOn, toList, (&))

uniqueWords :: [Text] -> [Text]
uniqueWords = map head . group . sort

-- use Set internally
uniqueWordsSet :: [Text] -> [Text]
uniqueWordsSet = toList . foldMap S.singleton

-- keep order of appearance , do not use Set
uniqueWithOrder :: [Text] -> [Text]
uniqueWithOrder = fmap snd . sortOn fst . fmap head . groupBy ((==) `on` snd) . sortOn snd . zip [0 :: Int ..]

-- should we not trust sortOn on order for repetitions
uniqueWithOrder2 :: [Text] -> [Text]
uniqueWithOrder2 = fmap snd . sortOn fst . fmap (minimumBy (comparing fst)) . groupBy ((==) `on` snd) . sortOn snd . zip [0 :: Int ..]

-- keep appearence order , use Set ,no Monad State
uniqueWithOrderSet :: [Text] -> [Text]
uniqueWithOrderSet = error "not implemented"

-- keep appearence order , use Set , use Monad State
uniqueWithOrderSetState :: [Text] -> [Text]
uniqueWithOrderSetState = error "not implemented"

-- use a Map Text Int to accumulate
extractVocabMap :: [Text] -> [(Text, Int)]
extractVocabMap xs = M.assocs $ foldl' @[] (&) mempty $ error "expression of xs"
