{-# LANGUAGE TypeApplications #-}
module Chapter_1.Unsolved.Vocabulary where

import qualified Data.Map.Strict as M
import Protolude (Text, foldl', group, sort, (&), toList, notImplemented)
import qualified Data.Set as S

uniqueWords :: [Text] -> [Text]
uniqueWords = map head . group . sort

-- use Set internally
uniqueWordsSet :: [Text] -> [Text]
uniqueWordsSet = notImplemented

-- keep order of appearance , do not use Set
uniqueWithOrder :: [Text] -> [Text]
uniqueWithOrder = notImplemented 

-- keep appearence order , use Set ,no Monad State
uniqueWithOrderSet :: [Text] -> [Text]
uniqueWithOrderSet = notImplemented

-- keep appearence order , use Set , use Monad State
uniqueWithOrderSetState :: [Text] -> [Text]
uniqueWithOrderSetState = notImplemented

-- use a Map Text Int to accumulate
extractVocabMap :: [Text] -> [(Text, Int)]
extractVocabMap xs = M.assocs $ foldl' @ [] (&) mempty $ notImplemented xs
