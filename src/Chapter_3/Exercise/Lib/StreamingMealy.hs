{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
module Chapter_3.Exercise.Lib.StreamingMealy where 
import Streaming
import qualified Streaming.Prelude as S
import qualified Control.Scanl as SL
import qualified Control.Foldl as L
import Protolude
import Control.Arrow ((>>>),(&&&))

-- | support for mealy type scanners (see 'Control.Scanl' from foldl)
mealy :: Monad m => (s -> a -> (s, b)) -> s -> Stream (Of a) m () -> Stream (Of b) m ()
mealy step s q =
  effect $ S.next q <&> either
      do pure
      do
        \(x, rest) ->
          let (s', x') = step s x
           in S.yield x' >> mealy step s' rest

track :: IO ()
track = S.print $ SL.purely_ mealy trackMean S.readLn
  where
    trackMean = ps L.mean >>> (ps L.minimum &&& ps L.maximum)
    ps = SL.postscan
