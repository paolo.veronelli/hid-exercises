{-# LANGUAGE DeriveFunctor #-}

module Chapter_3.Unsolved.Lardo where

import Control.Lens (Profunctor (..))
import Protolude (notImplemented)

newtype Lardo f a b = Lardo (a -> f b) deriving (Functor)

mkLardo :: Applicative f => (a -> b) -> Lardo f a b
mkLardo = notImplemented

instance Monoid (f b) => Monoid (Lardo f a b) where
  mempty = notImplemented

instance Semigroup (f b) => Semigroup (Lardo f a b) where
  Lardo f <> Lardo g = notImplemented

instance Functor f => Profunctor (Lardo f) where
  lmap f (Lardo g) = notImplemented
  rmap = notImplemented
