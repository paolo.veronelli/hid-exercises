{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Chapter_3.Unsolved.Mealy where

import Control.Arrow (Arrow (arr, first), (&&&))
import Control.Category
import Control.Foldl (Fold (Fold))
import qualified Control.Foldl as L
import Protolude hiding (first, (.))
import Control.Comonad

data Mealy a b where
  Mealy :: (x -> a -> (x, b)) -> x -> Mealy a b

deriving instance Functor (Mealy a)

instance Applicative (Mealy a) where 
  pure b = Mealy 
    do \_ _ -> ((), b)
    do ()
  Mealy f x <*> Mealy g y = Mealy 
    do
      \(xf, xg) a ->
        let (xf', fb) = f xf a
            (xg', b) = g xg a
         in ((xf', xg'), fb b)
    do (x, y)
    
mealy :: Fold a b -> Mealy a b
mealy (Fold f x g) = Mealy
  do \x a -> identity &&& g $ f x a
  do x

mealy2 :: Fold a b -> Mealy a b
mealy2 m  = Mealy
  do \m a -> id &&& extract $ extend (`L.fold` [a]) m 
  do m

moore :: Mealy a b -> b -> Fold a b
moore (Mealy f x) b = Fold
  do \(x, _) a -> f x a
  do (x, b)
  do snd

instance Category Mealy where
  Mealy f x . Mealy g y = Mealy
    do
      \(xg, xf) a ->
        let (xg', b) = g xg a
            (xf', c) = f xf b
         in ((xg', xf'), c)
    do (y, x)
  id = Mealy (,) ()

minOf :: Ord b => Fold a b -> Fold a (Maybe b)
minOf f = moore
  do mealy L.minimum . mealy f
  do Nothing 

instance Arrow Mealy where
  arr f = Mealy
    do const $ (,) () . f
    do ()
  first (Mealy f x) = Mealy
    do \x' (a, c) -> second (,c) $ f x' a
    do x

minAndMaxOf :: Ord b => Fold a b  -> Fold a (Maybe (b, b))
minAndMaxOf f  = uncurry (liftA2 (,)) <$> moore 
  do mealy f >>> mealy L.minimum &&& mealy L.maximum
  do (Nothing, Nothing)