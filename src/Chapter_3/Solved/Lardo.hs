{-# LANGUAGE DeriveFunctor #-}

module Chapter_3.Solved.Lardo where

import Control.Lens (Profunctor (..))

newtype Lardo f a b = Lardo (a -> f b) deriving (Functor)

mkLardo :: Applicative f => (a -> b) -> Lardo f a b
mkLardo = Lardo . fmap pure

instance Monoid (f b) => Monoid (Lardo f a b) where
  mempty = Lardo $ const mempty

instance Semigroup (f b) => Semigroup (Lardo f a b) where
  Lardo f <> Lardo g = Lardo $ (<>) <$> f <*> g

instance Functor f => Profunctor (Lardo f) where
  lmap f (Lardo g) = Lardo (g . f)
  rmap = fmap

