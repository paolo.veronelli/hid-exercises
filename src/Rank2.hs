{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Rank2 where

import Data.String (String)
import Protolude

rank2Show :: (forall a. Typeable a => a -> String) -> String
rank2Show yourshow = yourshow (1 :: Int) ++ " / " ++ yourshow ("and more" :: String) ++ "."

coolshow :: Typeable a => a -> String
coolshow a = case cast a :: Maybe Int of
  Just _ -> "THIS IS AN INT"
  Nothing -> case cast a :: Maybe String of
    Just _ -> "THIS IS A STRING"
    Nothing -> "DUNNO"

coolshow2 :: Typeable a => a -> String
coolshow2 x = fromMaybe "DUNNO" $ 
  cast @_ @Int x $> "THIS IS AN INT" <|> 
    cast @_ @String x $> "THIS IS A STRING"

main :: IO ()
main = putStrLn $ rank2Show coolshow2
